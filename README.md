# Fordocu

Fordocu is a documentation generating tool for Fortran source files. This
application is developed by VORtech acting upon instructions from 'Planbureau
voor de Leefomgeving' (PBL). Fordocu is inspired by the open source codes
F90doc and Doxygen.

If you have questions about the source of Fordocu you can contact VORtech.

+ VORtech : http://www.vortech.nl
+ PBL     : http://www.planbureauvoordeleefomgeving.nl
+ doxygen : http://www.doxygen.org
+ f90doc  : http://erikdemaine.org/software/f90doc/

# Installation
Fordocu required perl and the package XML::Simple. 

To install XML::Simple, copy and paste the appropriate command in to your terminal.

## cpanm
```bash
cpanm XML::Simple
```

## CPAN shell
```bash
perl -MCPAN -e shell
install XML::Simple
```

For more information on module installation, please visit the detailed 
[CPAN module installation guide](https://www.cpan.org/modules/INSTALL.html).
